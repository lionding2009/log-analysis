package org.log.analysis;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.*;

class JobTest {

    @Test
    void readFile_givenValidPath_shouldReturnLines() throws IOException {
        Job job = new Job("file.log");
        assertEquals(job.getLines().size(), 23);
    }

    @Test
    void readFile_givenInvalidPath_shouldThrowIOException() {
        assertThrows(IOException.class, () -> {
            Job job = new Job("no.log");
        });
    }

    @Test
    void parseLines_givenDefaultPattern_shouldBeParsed() throws IOException {
        Job job = new Job("file.log");;
        job.parseLines(job.getLinePattern());
        assertEquals(job.getLines().get(0).getSegments().size(), 9);
    }

    @Test
    void parseLines_givenWrongPattern_shouldNotBeParsed() throws IOException {
        Job job = new Job("file.log");
        job.parseLines(Pattern.compile("^asdfd"));
        assertEquals(job.getLines().get(0).getSegments().size(), 0);
    }

    @Test
    void getSegmentsBy_givenIndex_shouldReturnSegments() throws IOException {
        Job job = new Job("file.log");
        assertEquals(job.getSegmentsBy(0).size(), 23);
    }

    @Test
    void getSegmentsBy_givenWrongIndex_shouldReturnNoSegments() throws IOException {
        Job job = new Job("file.log");
        assertEquals(job.getSegmentsBy(20).size(), 0);
    }

    @Test
    void getUniqueIPs_givenSegments_shouldReturnAllUniques() throws IOException {
        Job job = new Job("file.log");
        List<Segment> segments = List.of(
                new Segment("172.168.0.1", 0),
                new Segment("172.168.0.2", 0),
                new Segment("172.168.0.3", 0),
                new Segment("172.168.0.3", 0),
                new Segment("172.168.0.4", 0));
        Set<String> IPs = job.getUniqueIPs(segments);
        assertEquals(IPs.size(), 4);
    }

    @Test
    void getUniqueIPs_givenNoSegments_shouldReturnNoIP() throws IOException {
        Job job = new Job("file.log");;
        List<Segment> segments = List.of();
        Set<String> IPs = job.getUniqueIPs(segments);
        assertEquals(IPs.size(), 0);
    }

    @Test
    void getIPsAndCounts_givenSegments_shouldReturnIPAndCounts() throws IOException {
        Job job = new Job("file.log");
        List<Segment> segments = List.of(
                new Segment("172.168.0.1", 0),
                new Segment("172.168.0.2", 2),
                new Segment("172.168.0.3", 0),
                new Segment("172.168.0.3", 0),
                new Segment("172.168.0.4", 1));
        LinkedHashMap<Integer, List<String>> IPs = job.getIPsAndCounts(segments);

        assertEquals(IPs.get(2), List.of("172.168.0.3"));
        assertEquals(IPs.get(1).size(), 3);
    }

    @Test
    void getIPsAndCounts_givenNoSegments_shouldReturnEmpty() throws IOException {
        Job job = new Job("file.log");
        List<Segment> segments = List.of();
        LinkedHashMap<Integer, List<String>> IPs = job.getIPsAndCounts(segments);

        assertNull(IPs.get(2));
    }

    @Test
    void getURLsAndCounts_givenSegments_shouldReturnURLAndCounts() throws IOException {
        Job job = new Job("file.log");
        List<Segment> segments = List.of(
                new Segment("www.test.com", 0),
                new Segment("www.google.com", 0),
                new Segment("www.test.com", 0),
                new Segment("www.abc.com", 1));
        LinkedHashMap<Integer, List<String>> IPs = job.getIPsAndCounts(segments);

        assertEquals(IPs.get(2), List.of("www.test.com"));
        assertEquals(IPs.get(1).size(), 2);
    }

    @Test
    void getURLsAndCounts_givenNoSegments_shouldReturnEmpty() throws IOException {
        Job job = new Job("file.log");
        List<Segment> segments = List.of();
        LinkedHashMap<Integer, List<String>> IPs = job.getIPsAndCounts(segments);

        assertNull(IPs.get(2));
    }


}
