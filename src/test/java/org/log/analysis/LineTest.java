package org.log.analysis;

import org.junit.jupiter.api.Test;

import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.*;

class LineTest {



    @Test
    void parse_givenALineHasAPattern_shouldParsed() {

        Line line = new Line("abc");
        line.parse(Pattern.compile("^abc"));
        assertEquals(line.getSegments().size(), 1);

    }

    @Test
    void parse_givenALineHasNoPattern_shouldEmpty() {

        Line line = new Line("abc");
        line.parse(Pattern.compile("^bcd"));
        assertEquals(line.getSegments().size(), 0);

    }

    @Test
    void parse_givenNoLineHasAPattern_shouldReturnNull() {

        Line line = new Line("");
        line.parse(Pattern.compile("^bcd"));
        assertEquals(line.getSegments().size(), 0);

    }

    @Test
    void parse_givenNull_shouldReturnNull() {

        Line line = new Line(null);
        line.parse(Pattern.compile("^bcd"));
        assertNull(line.getSegments());

    }
}
