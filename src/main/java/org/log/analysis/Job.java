package org.log.analysis;

import lombok.Data;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Data
public class Job {

    private List<Line> lines;
    private Pattern linePattern = Pattern.compile("^((?:\\d+\\.){3}\\d+)|\\s+(\\w+|\\-)|\\[(.*?)\\]|\"(.*?)\"");

    public Job(String filePath) throws IOException {
        this.lines = readFile(filePath);
        this.parseLines(linePattern);
    }

    public List<Line> readFile(String filePath) throws IOException {
        Path path = Paths.get(filePath);
        List<Line> lines = new ArrayList<>();
        BufferedReader reader = Files.newBufferedReader(path);
        String line;
        while ((line = reader.readLine()) != null) {
            lines.add(new Line(line));
        }
        return lines;
    }

    public void parseLines(Pattern linePattern) {
        this.lines.forEach(line -> line.parse(linePattern));
    }

    public Set<String> getUniqueIPs(List<Segment> segments) {
        Set<String> uniqueIPs = new HashSet<>();
        segments.forEach(segment -> uniqueIPs.add(segment.getContent()));
        return uniqueIPs;
    }

    public LinkedHashMap<Integer, List<String>> getIPsAndCounts(List<Segment> segments) {
        HashMap<String, Integer> map = this.countByContent(segments);
        return sortAndGroupByNumber(map);
    }

    public LinkedHashMap<Integer, List<String>> getURLsAndCounts(List<Segment> segments) {
        HashMap<String, Integer> map = this.countByContent(segments);
        return sortAndGroupByNumber(map);
    }

    private HashMap<String, Integer> countByContent(List<Segment> segments) {
        HashMap<String, Integer> map = new HashMap<>();
        segments.forEach(segment -> {
            if (null == map.get(segment.getContent()) || map.get(segment.getContent()).equals(0)) {
                map.put(segment.getContent(), 1);
            } else {
                map.put(segment.getContent(), map.get(segment.getContent()) + 1);
            }

        });
        return map;
    }

    private LinkedHashMap<Integer, List<String>> sortAndGroupByNumber(HashMap<String, Integer> map) {
        LinkedHashMap<Integer, List<String>> sortedMap = new LinkedHashMap<>();
        map.entrySet().stream().sorted((k1, k2) -> -k1.getValue().compareTo(k2.getValue())).forEach(item -> {
            if (sortedMap.get(item.getValue()) == null || sortedMap.get(item.getValue()).isEmpty()) {
                List<String> temp = new ArrayList<>();
                temp.add(item.getKey());
                sortedMap.put(item.getValue(), temp);
            } else {
                sortedMap.get(item.getValue()).add(item.getKey());
            }
        });
        return sortedMap;
    }

    public List<Segment> getSegmentsBy(Integer index){
        return lines.stream().map((line ->
                line.getSegments()
                        .stream()
                        .filter(segment ->
                                segment.getIndex().equals(index)).collect(Collectors.toList())))
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }


}
