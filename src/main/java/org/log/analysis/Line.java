package org.log.analysis;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Data
public class Line {

    private String line;
    private List<Segment> segments;

    public Line(String line) {
        this.line = line;
    }

    public void parse(Pattern linePattern) {
        if (null == line) {
            return;
        }
        segments = new ArrayList<>();
        Matcher matcher = linePattern.matcher(line);
        boolean result = matcher.find();
        int index = 0;
        while (result) {
            segments.add(new Segment(matcher.group(), index));
            result = matcher.find();
            index ++;
        }
    }


}
