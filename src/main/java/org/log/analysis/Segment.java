package org.log.analysis;

import lombok.Data;

@Data
public class Segment {


    public Segment(String content, Integer index) {
        this.content = content;
        this.index = index;
    }

    private String content;
    private Integer index;


}
