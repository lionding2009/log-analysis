package org.log.analysis;

import java.io.IOException;
import java.util.*;

public class App {

    public static void main(String[] args) {
        try {
            doJob("file.log");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void doJob(String filePath) throws IOException {
        Job job = new Job(filePath);
        getUniqueIPs(job);
        getTop3ActiveIPs(job);
        getTop3VisitedURLs(job);
    }

    public static void getUniqueIPs(Job job) {
        System.out.println(job.getUniqueIPs(job.getSegmentsBy(0)).size());
    }

    public static void getTop3ActiveIPs(Job job) {
        getContentsByLimit(job.getIPsAndCounts(job.getSegmentsBy(0)), 3);
    }

    public static void getTop3VisitedURLs(Job job) {
        getContentsByLimit(job.getURLsAndCounts(job.getSegmentsBy(4)), 3);
    }

    private static void getContentsByLimit(LinkedHashMap<Integer, List<String>> contents, Integer limit) {
        Iterator<Map.Entry<Integer, List<String>>> iterator = contents.entrySet().iterator();
        int count = 0;
        while (iterator.hasNext() && count <= limit) {
            System.out.println(iterator.next().getValue());
            count ++;
        }
    }


}
